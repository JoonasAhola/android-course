package viewsandlayouts;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

public class SmileyView extends View {
	
	public SmileyView(Context context) {
		super(context);
	}
	
	public SmileyView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	public SmileyView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}
	
	public void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		
		Paint yellow = new Paint();
		yellow.setColor(Color.YELLOW);
		yellow.setAntiAlias(true);
		
		Paint black = new Paint();
		black.setColor(Color.BLACK);
		black.setAntiAlias(true);
		
		canvas.drawCircle(110, 110, 100, yellow);
		canvas.drawCircle(75, 100, 20, black);
		canvas.drawCircle(145, 100, 20, black);
		
		black.setStrokeWidth(20);
		black.setStrokeCap(Paint.Cap.ROUND);
		black.setStyle(Paint.Style.STROKE);
		
		RectF rectf = new RectF(40,40,180,180);
		
		canvas.drawArc(rectf, 20, 140, false, black);
	}
}
