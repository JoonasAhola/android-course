package com.example.intents;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.TextView;

public class SecondActivity extends Activity {
	TextView sumText;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_second);
		sumText = (TextView) findViewById(R.id.sumValueTextView);
		Bundle extras = getIntent().getExtras();
		if(extras != null) {
			int value = extras.getInt("value");
			
			sumText.setText(Integer.toString(value));
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.second, menu);
		return true;
	}

}
