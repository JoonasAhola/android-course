package com.example.intents;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	int value1 = 0;
	int value2 = 0;
	EditText field;
	TextView firstValueText;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		field = (EditText) findViewById(R.id.sumEditField);
		firstValueText = (TextView) findViewById(R.id.firstValueText);
		addButtonListener();
	}

	private void addButtonListener() {
		Button btMessage = (Button)findViewById(R.id.calculateSumButton);
		
		btMessage.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String sValue = null;
				int value = 0;
				sValue = field.getText().toString();
				if(sValue != null && !sValue.isEmpty()) {
					value = Integer.parseInt(sValue);
					if(value > 0) {
						calculateButtonSum(value);
						field.setText("");
						firstValueText.setText(sValue);
					}else {
						showToast("Number must be greater than 0");
					}
					
				}
			}
		});
	}
	
	private void calculateButtonSum(int value) {
		int sumValue = 0;
		if(value1 < 1) {
			value1 = value;
		}else if(value2 < 1) {
			value2 = value;
			sumValue = value1 + value2;
			changeActivity(sumValue);
			value1 = 0;
			value2 = 0;
			firstValueText.setText("");
			field.setText("");
		}
	}

	private void changeActivity(int sumValue) {
		Intent changeActivity = new Intent(this, SecondActivity.class);
		changeActivity.putExtra("value", sumValue);
		startActivity(changeActivity);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void showToast(String toastText) {
    	Context context = getApplicationContext();
    	int duration = Toast.LENGTH_LONG;
    	Toast toast = Toast.makeText(context, toastText, duration);
    	toast.show();
    	
    }
	
}
