package com.example.telephony;


import android.net.Uri;
import android.os.Bundle;
import android.telephony.*;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {
	EditText numberField;
	String number = "";
	EditText smsContentField;
	EditText smsNumberField;
	TelephonyManager manager;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		numberField = (EditText) findViewById(R.id.numberToCallField);
		smsContentField = (EditText) findViewById(R.id.smsEditText);
		smsNumberField = (EditText) findViewById(R.id.smsNumberToSend);
		connectToTelephony();
		
		addButtonListeners();
	}
	
    private PhoneStateListener phoneStateListener = new PhoneStateListener() {
		public void onCallStateChanged(int state, String incomingNumber) {
	        switch(state)
	        {
	            case TelephonyManager.CALL_STATE_RINGING:
	                showToast("Incoming call");
	                break;
                default:
                	break;

	        }	
		}
	};
	/*
	private BroadcastReceiver smsReceiver = new BroadcastReceiver() {
		public void onReceive(Context context, Intent intent) {
			
		}
		
	}
	*/
	
	
	private void connectToTelephony() {
		manager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
		int callstate = manager.getCallState();
		
		manager.listen(phoneStateListener , PhoneStateListener.LISTEN_CALL_STATE);
		showToast("callstate is "+callstate);
		
	}
	
	

	private void addButtonListeners() {
		Button callButton = (Button) findViewById(R.id.callPhoneButton);
		callButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				parseAndCallEnterdNumber();
			}
		});
		
		Button smsButton = (Button) findViewById(R.id.sendSMSButton);
		smsButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String number =  smsNumberField.getText().toString();
				String content = smsContentField.getText().toString();
				if(number != null && !number.isEmpty() && content != null && !content.isEmpty() && content.length() < 181){
					sendSMS(number, content);
				}else {
					showToast("SMS-number or content invalid!, number: "+number+", content: "+content);
				}
			}
		});
		
	}
	

	private void sendSMS(String number, String content) {
		SmsManager smsm = SmsManager.getDefault();
		smsm.sendTextMessage(number, null, content, null, null);
		
	}
	
	private void parseAndCallEnterdNumber() {
		number = "tel:"+numberField.getText().toString();
		try {
			Intent callIntent = new Intent(Intent.ACTION_CALL);
			callIntent.setData(Uri.parse(number));
			startActivity(callIntent);
		} catch(Exception e) {
			showToast("Exception when trying to call the number!");
		}
		
	}

	public void showToast(String toastText) {
    	Context context = getApplicationContext();
    	int duration = Toast.LENGTH_LONG;
    	Toast toast = Toast.makeText(context, toastText, duration);
    	toast.show();
    	
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
