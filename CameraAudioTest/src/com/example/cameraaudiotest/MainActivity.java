package com.example.cameraaudiotest;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import android.hardware.Camera;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		//create a view for our camera
		final CameraView cameraView = new CameraView(getApplicationContext());
		
		//set the preview frame (put the cameraview to the frame)
		FrameLayout frame = (FrameLayout)findViewById(R.id.previewPictureFrame);
		frame.addView(cameraView);
		
		Button paper = (Button)findViewById(R.id.btSavePicture);
		
		paper.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				cameraView.capture(new Camera.PictureCallback() {
					
					@Override
					public void onPictureTaken(byte[] data, Camera camera) {
						Log.d("My Camera", "Image received from the camera");
						System.out.println("Image Received");
						ByteArrayInputStream baits = new ByteArrayInputStream(data);
						
						try {
							setWallpaper(baits);
						} catch (IOException e) {
							Log.d("My Camera", "Couldn't save picture to wallpaper!");
							e.printStackTrace();
						}
						
					}
				});
				
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
