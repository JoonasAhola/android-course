package com.example.shoppinglist;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class NewShoppingItem extends Activity {


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_newshoppingitem);
		addButtonListener();
	}
	
	
	private void addButtonListener() {
		Button btMessage = (Button)findViewById(R.id.addNewItemButton);
		btMessage.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				EditText editArea = (EditText)findViewById(R.id.newShoppingItemTextfield);
				String newItem = null;
				newItem = editArea.getText().toString(); 
				ShoppingList.shoppings.add(newItem);
				finish();
			}
		});
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.shoppinglist, menu);
		return true;
	}
}
