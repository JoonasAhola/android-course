package com.example.shoppinglist;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;

public class ShoppingList extends ListActivity {
	public static ArrayList<String> shoppings = new ArrayList<String>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_shoppinglist);
		addFromSharedPrefs();
		updateShoppingList();
		
	}
	
	
	
	private void addFromSharedPrefs() {
		SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
		int amountOfShoppings = prefs.getInt("maara", 0);
		for(int i=0;i<amountOfShoppings;i++) {
			if(!prefs.getString("item-"+i, "virhe").equalsIgnoreCase("virhe")){
				if(!shoppings.contains(prefs.getString("item-"+i, "virhe"))) {
					shoppings.add(prefs.getString("item-"+i, "virhe"));
				}
			}
		}
	}



	private void updateShoppingList() {
		if(shoppings == null || shoppings.size() == 0) {
			shoppings.add("Leip��");
			shoppings.add("Maitoa");
		}
		setListAdapter(new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, shoppings.toArray(new String[1])));
	}
	
	protected void onResume() {
		super.onResume();
		updateShoppingList();
	}
	
	protected void onPause() {
		super.onPause();
		SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();
		for(int i=0;i<shoppings.size();i++) {
			editor.putString("item-"+i, shoppings.get(i));
		}
		editor.putInt("maara", shoppings.size());
		editor.commit();
	}



	public void launchNewShoppingItem() {
		Intent newShoppingItem = new Intent(this, NewShoppingItem.class);
		startActivity(newShoppingItem);
	}
	
	private void launchInstructions() {
		Intent instructions = new Intent(this, Instructions.class);
		startActivity(instructions);
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
		
		switch(item.getItemId()) {
			case R.id.newShoppingItemMenuButton:
				launchNewShoppingItem();
				break;
			case R.id.instuctionsMenuButton:
				launchInstructions();
				break;
			default:
				break;
		}
		
		return super.onOptionsItemSelected(item);
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.shoppinglist, menu);
		return true;
	}

}
