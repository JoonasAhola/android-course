package com.example.shoppinglist;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

public class Instructions extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_instructions);
		WebView instructionWebView;
		instructionWebView = (WebView)findViewById(R.id.instructionsViewView);
		instructionWebView.loadUrl("file:///android_asset/instructions.html");
		
		Button btMessage = (Button)findViewById(R.id.InstructionsWebviewCloseButton);
		
		btMessage.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.shoppinglist, menu);
		return true;
	}

}
