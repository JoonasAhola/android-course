package com.example.location;

import java.util.List;
import java.util.Locale;

import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.view.Menu;
import android.widget.Toast;

public class MainActivity extends Activity {
	Location lastLocation;
	private LocationListener locationListener = new LocationListener() {
		
		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			switch(status) {
				case(0):
					showToast("Current provider("+provider+") out of service");
					break;
				case(1):
					showToast("Current provider("+provider+") temporarily unavailable");
					break;
				case(2):
					showToast("Provider "+provider+" available");
					break;
				default:
					break;
			}
				
		}
		
		@Override
		public void onProviderEnabled(String provider) {
			showToast("Location provider enabled: "+provider);
			
		}
		
		@Override
		public void onProviderDisabled(String provider) {
			showToast("Location provider disabled: "+provider);
			
		}
		
		@Override
		public void onLocationChanged(Location location) {
				if(lastLocation != null) {
					showToast("Latitude: "+location.getLatitude()+ ", Longitude: "+location.getLongitude()+
							", Altitude: "+location.getAltitude()+ ","+
							" distance to last location:" +location.distanceTo(lastLocation)+" meters");
				}else {
					showToast("Latitude: "+location.getLatitude()+ ", Longitude: "+location.getLongitude()+
							", Altitude: "+location.getAltitude());
				}
				lastLocation = new Location(location);
			
		}
	};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initLocationManager();
	}

	private void initLocationManager() {
		LocationManager lmgr = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		showToast(lmgr.getProviders(true).toString());
		Criteria criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_FINE);
		String provider = lmgr.getBestProvider(criteria, true);
		lmgr.requestLocationUpdates(provider, 2000, 0, locationListener);
		
	}
	
	public void showToast(String toastText) {
    	Context context = getApplicationContext();
    	int duration = Toast.LENGTH_LONG;
    	Toast toast = Toast.makeText(context, toastText, duration);
    	toast.show();
    	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
